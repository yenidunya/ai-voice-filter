import json
import argparse
from os import path

import numpy as np
from scipy.io import wavfile
from pydub import AudioSegment

parser = argparse.ArgumentParser(description='Read file and process audio')
parser.add_argument('wav_file', type=str, help='File to read and process')

save_folder = "saves"
log_folder = "logs"

def get_data(audio):
    sr = audio.frame_rate
    data = convert_np(audio)
    return sr, data

def convert_np(audio):
    sample = np.fromstring(audio.raw_data, np.int16)
    templist = list()
    index = 0
    while index < len(sample):
        templist.append([sample[index], sample[index + 1]])
        index = index + 2
    
    return np.array(templist)

def split_file(filepath, part_len):
    audio = AudioSegment.from_wav(filepath)
    audio_len = len(audio)
    audiolist = list()

    index = 0
    # counter = 1
    while(index < audio_len and audio_len > part_len):
        part = audio[index: index + part_len]
        # save_audio(part, "Part "+str(counter)+".wav")
        audiolist.append(part)
        index = index + part_len
        # counter = counter + 1
    
    return audiolist

def process_sound(audio):
    sr, data = get_data(audio)

    if data.dtype != np.int16:
        raise TypeError('Bad sample type: %r' % data.dtype)

    # local import to reduce start-up time
    from audio.processor import WavProcessor, format_predictions

    with WavProcessor() as proc:
        predictions = proc.get_predictions(sr, data)

    return format_predictions(predictions)

def save_audio(audiofile, name):
    audiofile.export(path.join(save_folder, name), format="wav")
    
def open_log():
    filename = "log1"

    while path.exists(path.join(log_folder, filename)):
        name = filename[0:3]
        number = int(filename[3:])
        filename = name + str(number + 1)
    print("Writing on " + filename)
    
    return open(path.join(log_folder, filename), 'w')

def write_log(log, message):
    print(message)

    if type(message) == dict:
        log.write(json.dumps(message)+"\n")
    else:
        log.write(message+"\n")

def get_predictions(filepath, part_len = 1000):
    # log = open_log()
    predictions = list()
    
    # write_log(log, "Whole Audio Prediction : ")
    whole_prediction = process_sound(AudioSegment.from_wav(filepath))
    predictions.append(whole_prediction)
    # write_log(log, whole_prediction)
    
    sounds = split_file(filepath, part_len)
    part_number = 1
    for sound in sounds:
        prediction = process_sound(sound)
        predictions.append(prediction)
        
        # write_log(log, "\nPart " + str(part_number) + " : ")
        # write_log(log, prediction)

        part_number = part_number + 1
    
    # log.close()
    return json.dumps(predictions)

if __name__ == '__main__':
    args = parser.parse_args()
    filepath = args.wav_file

    output = get_predictions(filepath)
    print("Result : \n" + output)